# PROVIDER
provider "aws" {
  region = var.region
}

resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = file(var.aws_public_key_ssh_path)
}

resource "aws_default_vpc" "VirtualPrivateNetwork" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "ssh_web" {
  vpc_id = aws_default_vpc.VirtualPrivateNetwork.id

  # Ingress rules
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Egress rule allowing all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
}

resource "aws_instance" "ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "admin"

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.admin]
}